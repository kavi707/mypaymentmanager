package com.payment.manager.transactions;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 2/13/13
 * Time: 8:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class Transaction {

    private long id;
    private String name;
    private String dateTime;
    private String type; // income or outcome
    private float amount;
    private String details;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Transaction(String TrcName) {
        name = TrcName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
