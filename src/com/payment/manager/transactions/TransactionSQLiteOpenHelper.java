package com.payment.manager.transactions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 2/13/13
 * Time: 8:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionSQLiteOpenHelper extends SQLiteOpenHelper  {

    private SQLiteDatabase sqLiteDatabase;
    private ArrayList<Transaction> availableTransactions = null;

    public static final String DB_NAME = "money_transaction.sqlite";
    public static final int VERSION = 1;

    public static final String TABLE_NAME = "transactions";
    public static final String TRC_ID = "trc_id";
    public static final String TRC_NAME = "trc_name";
    public static final String TRC_DATE_TIME = "date_time";
    public static final String TRC_TYPE = "trc_type";
    public static final String TRC_AMOUNT = "trc_amount";
    public static final String TRC_DETAILS = "trc_details";

    public TransactionSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createTable(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    private void createTable(SQLiteDatabase sqLiteDatabase) {

        String createTableQuery = "create table " + TABLE_NAME + " ( " +
                TRC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                TRC_NAME + " text, " +
                TRC_DATE_TIME + " text, " +
                TRC_TYPE + " text, " +
                TRC_AMOUNT + " real, " +
                TRC_DETAILS + " real " +
                ");";
        sqLiteDatabase.execSQL(createTableQuery);
    }

    public void addTransaction(Transaction transaction){

        sqLiteDatabase = this.getWritableDatabase();

        assert(transaction != null);
        ContentValues values = new ContentValues();
        values.put(TRC_NAME, transaction.getName());
        values.put(TRC_DATE_TIME, transaction.getDateTime());
        values.put(TRC_TYPE, transaction.getType());
        values.put(TRC_AMOUNT, transaction.getAmount());
        values.put(TRC_DETAILS, transaction.getDetails());

        try{
            long id = sqLiteDatabase.insert(TABLE_NAME, null, values);
            transaction.setId(id);
        } catch (SQLiteException ex){
            throw ex;
        }
    }

    public ArrayList<Transaction> loadTransactions(String selectQry) {

        availableTransactions = new ArrayList<Transaction>();
        sqLiteDatabase = this.getWritableDatabase();

        try{
            Cursor transactionCursor = sqLiteDatabase.rawQuery(selectQry, null);

            transactionCursor.moveToFirst();
            Transaction transaction;

            if(!transactionCursor.isAfterLast()) {

                do {
                    long id = transactionCursor.getLong(0);
                    String name = transactionCursor.getString(1);
                    String dateTime = transactionCursor.getString(2);
                    String type = transactionCursor.getString(3);
                    long amount = transactionCursor.getLong(4);
                    String details = transactionCursor.getString(5);

                    transaction = new Transaction(name);
                    transaction.setId(id);
                    transaction.setDateTime(dateTime);
                    transaction.setType(type);
                    transaction.setAmount(amount);
                    transaction.setDetails(details);

                    availableTransactions.add(transaction);
                } while(transactionCursor.moveToNext());
            }
            transactionCursor.close();

        } catch (SQLiteException ex){
            throw ex;
        }


        return availableTransactions;
    }

    public boolean removeTransactions(String deleteQuery){

        sqLiteDatabase = this.getWritableDatabase();
        boolean status = false;

        if(deleteQuery == "All"){
            try {
                sqLiteDatabase.delete(TABLE_NAME,null,null);
                status = true;
            } catch (SQLiteException ex){
                throw ex;
            }
        } else {

            String[] qryContent = deleteQuery.split(":");
            String where = null;

            if (qryContent[0].equals("name")){

                where = TRC_NAME + " = '" + qryContent[1] + "'";

                try {
                    sqLiteDatabase.delete(TABLE_NAME, where, null);
                    status = true;
                } catch (SQLiteException ex) {
                    throw ex;
                }
            } else if (qryContent[0].equals("id")) {

                where = TRC_ID + " = '" + qryContent[1] + "'";

                try {
                    sqLiteDatabase.delete(TABLE_NAME, where, null);
                    status = true;
                } catch (SQLiteException ex) {
                    throw ex;
                }
            } else {
                status = false;
            }
        }

        return status;
    }
}
