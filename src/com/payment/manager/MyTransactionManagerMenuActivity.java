package com.payment.manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import com.payment.manager.templates.*;

public class MyTransactionManagerMenuActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    public ImageButton paymentListButton;
//    public ImageButton paymentSearchButton;
    public ImageButton addPaymentButton;
    public ImageButton removePaymentButton;
    public ImageButton controllerButton;
    public ImageButton exitButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setUpView();
    }

    private void setUpView(){

        paymentListButton = (ImageButton)findViewById(R.id.payment_list_button);
//        paymentSearchButton = (ImageButton)findViewById(R.id.search_payment_button);
        addPaymentButton = (ImageButton)findViewById(R.id.add_payment_button);
        removePaymentButton = (ImageButton)findViewById(R.id.remove_payment_button);
        controllerButton = (ImageButton)findViewById(R.id.controller_button);
        exitButton = (ImageButton)findViewById(R.id.exit_button);

        paymentListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent(MyTransactionManagerMenuActivity.this, TransactionListActivity.class);
                startActivity(intent);
            }
        });

        /*paymentSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent(MyTransactionManagerMenuActivity.this, SearchTransactionActivity.class);
                startActivity(intent);
            }
        });*/

        addPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent(MyTransactionManagerMenuActivity.this, AddNewTransactionActivity.class);
                startActivity(intent);
            }
        });

        removePaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent(MyTransactionManagerMenuActivity.this, RemoveTransactionActivity.class);
                startActivity(intent);
            }
        });

        controllerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent(MyTransactionManagerMenuActivity.this, ControllerActivity.class);
                startActivity(intent);
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });
    }
}
