package com.payment.manager.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import com.payment.manager.R;
import com.payment.manager.transactions.TransactionSQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 2/12/13
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class RemoveTransactionActivity extends Activity {

    private RadioButton removeNameRadioButton;
    private EditText trcNameEditText;
    private RadioButton removeMonthRadioButton;
    private EditText trcIdEditText;
    private Button removeButton;
    private Button removeAllButton;

    private AlertDialog successRemoveDialog;
    private AlertDialog errorRemoveDialog;

    final Context context = this;

    private int event = 0;

    private TransactionSQLiteOpenHelper sqLiteOpenHelper = new TransactionSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.remove_transactions);

        setUpViews();
    }

    private void setUpViews() {

        removeNameRadioButton = (RadioButton)findViewById(R.id.nameRadioButton);
        trcNameEditText = (EditText)findViewById(R.id.trcNameEditText);
        removeMonthRadioButton = (RadioButton)findViewById(R.id.monthRadioButton);
        trcIdEditText = (EditText)findViewById(R.id.trcIdEditText);
        removeButton = (Button)findViewById(R.id.removeButton);
        removeAllButton = (Button)findViewById(R.id.removeAllButton);

        trcNameEditText.setEnabled(false);
        trcNameEditText.setFocusableInTouchMode(false);
        trcIdEditText.setEnabled(false);
        trcIdEditText.setFocusableInTouchMode(false);
        removeButton.setEnabled(false);

        removeNameRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trcNameEditText.setEnabled(true);
                trcNameEditText.setFocusableInTouchMode(true);
                trcIdEditText.setEnabled(false);
                trcIdEditText.setFocusable(false);
                trcIdEditText.setFocusableInTouchMode(false);
                removeButton.setEnabled(true);
                event = 1;
            }
        });

        removeMonthRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trcNameEditText.setEnabled(false);
                trcNameEditText.setFocusable(false);
                trcNameEditText.setFocusableInTouchMode(false);
                trcIdEditText.setEnabled(true);
                trcIdEditText.setFocusableInTouchMode(true);
                removeButton.setEnabled(true);
                event = 2;
            }
        });

        removeAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    sqLiteOpenHelper.removeTransactions("All");
                    successRemoveDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.success)
                            .setMessage("Delete All Records from the database")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            }).create();
                    successRemoveDialog.show();
                } catch (SQLiteException ex){

                    errorRemoveDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.error)
                            .setMessage("Database error occurred while deleting data from database")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    errorRemoveDialog.cancel();
                                }
                            }).create();
                    errorRemoveDialog.show();
                } catch (Exception ex) {
                    errorRemoveDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.error)
                            .setMessage("Error occurred while deleting data")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    errorRemoveDialog.cancel();
                                }
                            }).create();
                    errorRemoveDialog.show();
                }
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean deleteStatus = false;

                try {
                    if(event == 1){
                        String trcName = trcNameEditText.getText().toString();
                        deleteStatus = sqLiteOpenHelper.removeTransactions("name:" + trcName);
                        if(deleteStatus){

                            trcNameEditText.setText("");

                            successRemoveDialog = new AlertDialog.Builder(context)
                                    .setTitle(R.string.success)
                                    .setMessage("Successfully removed the '" + trcName + "' from transaction list")
                                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            successRemoveDialog.cancel();
                                        }
                                    }).create();

                            successRemoveDialog.show();
                        }
                    } else if(event == 2) {
                        String trcId = trcIdEditText.getText().toString();
                        deleteStatus = sqLiteOpenHelper.removeTransactions("id:" + trcId);

                        if(deleteStatus) {

                            trcIdEditText.setText("");

                            successRemoveDialog = new AlertDialog.Builder(context)
                                    .setTitle(R.string.success)
                                    .setMessage("Successfully removed the '" + trcId + "' from transaction list")
                                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            successRemoveDialog.cancel();
                                        }
                                    }).create();

                            successRemoveDialog.show();
                        }
                    }
                } catch (SQLiteException ex) {

                    Log.d("tag", "SQL Error:" + ex);
                    errorRemoveDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.error)
                            .setMessage("Database error occurred while deleting data from database")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    errorRemoveDialog.cancel();
                                }
                            }).create();
                    errorRemoveDialog.show();
                } catch (Exception ex) {

                    Log.d("tag", "Error:" + ex);
                    errorRemoveDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.error)
                            .setMessage("Error occurred while deleting data")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    errorRemoveDialog.cancel();
                                }
                            }).create();
                    errorRemoveDialog.show();
                }
            }
        });
    }
}
