package com.payment.manager.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.*;
import com.payment.manager.R;
import com.payment.manager.transactions.Transaction;
import com.payment.manager.transactions.TransactionSQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 2/12/13
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddNewTransactionActivity extends Activity {

    private EditText trcNameText;
    private DatePicker trcDatePicker;
    private TimePicker trcTimePicker;
    private RadioButton trcTypePayment;
    private RadioButton trcTypeIncome;
    private EditText trcAmount;
    private EditText trcDetails;
    private Button trcSubmitButton;
    private Button trcClearButton;

    private AlertDialog successAddingTransaction;
    private AlertDialog errorAddingTransaction;
    final Context context = this;

    private TransactionSQLiteOpenHelper sqLiteOpenHelper = new TransactionSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.add_transaction);

        setUpView();
    }

    private void setUpView(){

        trcNameText = (EditText)findViewById(R.id.nameEditText);
        trcDatePicker = (DatePicker)findViewById(R.id.datePicker);
        trcTimePicker = (TimePicker)findViewById(R.id.timePicker);
        trcAmount = (EditText)findViewById(R.id.amountEditText);
        trcDetails = (EditText)findViewById(R.id.detailsEditText);

        trcTypePayment = (RadioButton)findViewById(R.id.paymentRadioButton);
        trcTypeIncome = (RadioButton)findViewById(R.id.incomeRadioButton);

        trcSubmitButton = (Button)findViewById(R.id.submitButton);
        trcClearButton = (Button)findViewById(R.id.clearButton);


        trcClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trcNameText.setText("");
                trcAmount.setText("");
                trcDetails.setText("");

                Time now = new Time();
                now.setToNow();

                trcDatePicker.updateDate(now.year,now.month,now.monthDay);
                trcTimePicker.setCurrentHour(now.hour);
                trcTimePicker.setCurrentMinute(now.minute);

                trcTypePayment.setChecked(false);
                trcTypeIncome.setChecked(false);
            }
        });

        trcSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = trcNameText.getText().toString();

                int year = trcDatePicker.getYear();
                int month = trcDatePicker.getMonth() + 1;
                int day = trcDatePicker.getDayOfMonth();
                String date = year + "/" + month + "/" + day;

                int hour = trcTimePicker.getCurrentHour();
                int minute = trcTimePicker.getCurrentMinute();
                String time = hour + ":" + minute;

                String dateTime = date + " " + time;
                String amount = trcAmount.getText().toString();
                String details = trcDetails.getText().toString();

                String type = null;
                if(trcTypeIncome.isChecked() && !trcTypePayment.isChecked()){
                    type = "income";
                } else if(trcTypePayment.isChecked() && !trcTypeIncome.isChecked()){
                    type = "payment";
                }

                if(name == null || amount == null || type == null) {

                    errorAddingTransaction = new AlertDialog.Builder(context)
                            .setTitle(R.string.error)
                            .setMessage("Transaction Name, Transfer amount and Transaction Type are mandatory fields")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    errorAddingTransaction.cancel();
                                }
                            }).create();

                    errorAddingTransaction.show();
                } else {
                    Transaction transaction = new Transaction(name);
                    transaction.setDateTime(dateTime);
                    try {
                        transaction.setAmount(Float.parseFloat(amount));
                        transaction.setDetails(details);
                        transaction.setType(type);

                        sqLiteOpenHelper.addTransaction(transaction);

                        trcNameText.setText("");
                        trcAmount.setText("");
                        trcDetails.setText("");

                        successAddingTransaction = new AlertDialog.Builder(context)
                                .setTitle(R.string.success)
                                .setMessage("Successfully Added the new Transaction")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener(){

                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                }).create();
                        successAddingTransaction.show();
                    } catch (SQLiteException ex){
                        errorAddingTransaction = new AlertDialog.Builder(context)
                                .setTitle(R.string.error)
                                .setMessage("Error occurred while communicate with database")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        errorAddingTransaction.cancel();
                                    }
                                }).create();
                        errorAddingTransaction.show();
                    } catch (Exception ex){
                        errorAddingTransaction = new AlertDialog.Builder(context)
                                .setTitle(R.string.error)
                                .setMessage("Entered amount is not a value. Please check and re-enter")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        errorAddingTransaction.cancel();
                                    }
                                }).create();

                        errorAddingTransaction.show();
                    }

                }
            }
        });
    }
}
