package com.payment.manager.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.payment.manager.R;
import com.payment.manager.transactions.Transaction;
import com.payment.manager.transactions.TransactionSQLiteOpenHelper;
import static com.payment.manager.transactions.TransactionSQLiteOpenHelper.*;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 2/12/13
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionListActivity extends Activity {

    private TransactionSQLiteOpenHelper sqLiteOpenHelper = new TransactionSQLiteOpenHelper(this);
    private ArrayList<Transaction> availableTransactionList;
    private ListView trcListView;
    private ArrayAdapter<String> trcListAdapter;

    private AlertDialog emptyListDialog;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.transaction_list);

        setUpViews();
    }

    private void setUpViews() {

        trcListView = (ListView)findViewById(R.id.trcListView);
        ArrayList<String> trcNames = new ArrayList<String>();

        availableTransactionList = sqLiteOpenHelper.loadTransactions("SELECT * FROM " + TABLE_NAME);

        if(availableTransactionList.isEmpty()){

            emptyListDialog = new AlertDialog.Builder(context)
                    .setTitle(R.string.warning)
                    .setMessage("No records for View")
                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).create();
            emptyListDialog.show();
        } else {
            for (Transaction trc : availableTransactionList) {
                trcNames.add(trc.getName() + " - " + trc.getDetails() + " - " + trc.getDateTime());
            }

            trcListAdapter = new ArrayAdapter<String>(this, R.layout.trc_row_view, trcNames);
            trcListView.setAdapter(trcListAdapter);
        }

    }
}
